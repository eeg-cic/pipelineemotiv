import csv
import os

filename = input("Enter raw data file name (including .csv): ")
if filename[0] == "\"":
    filename = filename[1:-1]

epoch = input("Enter epoch (in seconds): ")
epoch = int(epoch)

hz = 125
f = 1/hz


#############################################################################
######################### Zero Padding ######################################
#############################################################################

print ("Zero padding...")

ifile = open(filename, "r")
reader = csv.reader(ifile, delimiter = ';')
ofile = open('OutputPadded.csv', 'w', newline = "")
writer = csv.writer(ofile, delimiter = ';')

rownum = 0
scribble = [""]*19
tray = [""]*19

for row in reader:
    scribble = tray
    tray = row
    if scribble[0]!="":
        writer.writerow(scribble)

    if rownum > 2:
        t1 = float(scribble[0].replace(',' , '.'))
        t2 = float(tray[0].replace(',' , '.'))
        d =  t2 - t1
        gap = int(d/f)
        if gap > 1:
            tm = float(scribble[1].replace(',' , '.'))
            for i in range (gap):
                temp = ["","","0","0","0","0","0","0","0","0","0","0","0","0","0","0","","",""]
                temp[0] = str(t1+(f*(i+1))).replace('.' , ',')
                temp[1] = str(tm+(8*(i+1))).replace('.' , ',')
                writer.writerow(temp)
        else:
            pass
    else:
        pass

    rownum = rownum + 1
    
writer.writerow(tray)

print ("Finished as OutputPadded.csv")




#############################################################################
######################### Delete Resp. Line #################################
#############################################################################

print ("Deleting response and music line...")

ifile = open("OutputPadded.csv", "r")
reader = csv.reader(ifile, delimiter = ';')
ofile = open('OutputTemp.csv', 'w', newline = "")
writer = csv.writer(ofile, delimiter = ';')

rownum = 0
scribble = [""]*19
backlog1 = [""]*19
backlog2 = [""]*19
backlog3 = [""]*19
for row in reader:
    scribble = backlog1
    backlog1 = backlog2
    backlog2 = backlog3
    backlog3 = row

    if backlog2[16] != "" and backlog2[17] == "":
        timebefore = float(backlog2[0].replace(',' , '.')) - float(backlog1[0].replace(',' , '.'))
        timeafter = float(backlog3[0].replace(',' , '.')) - float(backlog2[0].replace(',' , '.'))

        if timebefore<=timeafter:
            backlog1[16] = backlog2[16]
        else:
            backlog3[16] = backlog2[16]

    if scribble[3] == "" and scribble[17] == "":
        pass
    else:
        writer.writerow(scribble)
    
writer.writerow(backlog1)
writer.writerow(backlog2)
writer.writerow(backlog3)

ifile.close()
os.remove('OutputPadded.csv')

#############################################################################
########################### Delete Music Line ###############################
#############################################################################

ifile = open('OutputTemp.csv', "r")
reader = csv.reader(ifile, delimiter = ';')
ofile = open('Output1.csv', 'w', newline = "")
writer = csv.writer(ofile, delimiter = ';')

rownum = 0
flag = 0
scribble = [""]*19
backlog1 = [""]*19
backlog2 = [""]*19
backlog3 = [""]*19
for row in reader:
    scribble = backlog1
    backlog1 = backlog2
    backlog2 = backlog3
    backlog3 = row

    if backlog2[17] != "" and backlog2[16] == "" and rownum > 2:
        timebefore = float(backlog2[0].replace(',' , '.')) - float(backlog1[0].replace(',' , '.'))
        timeafter = float(backlog3[0].replace(',' , '.')) - float(backlog2[0].replace(',' , '.'))

        if timebefore<=timeafter:
            backlog1[17] = backlog2[17]
        else:
            backlog3[17] = backlog2[17]

    if scribble[3] == "" and scribble[16] == "":
        if rownum == 4:
            writer.writerow(scribble)
        pass
    else:
        writer.writerow(scribble)

    rownum+=1
    
writer.writerow(backlog1)
writer.writerow(backlog2)
writer.writerow(backlog3)

ifile.close()
os.remove('OutputTemp.csv')

print ("Finished as Output1.csv")

#############################################################################
########################### Keep Only Signal ################################
#############################################################################

print ("Keeping only EEG signal...")

ifile = open('Output1.csv', "r")
reader = csv.reader(ifile, delimiter = ';')
ofile = open('Output2.csv', 'w', newline = "")
writer = csv.writer(ofile, delimiter = '\t')

row_count = sum(1 for row in ifile) - 2
writer.writerow(["14", str(row_count), "128","","","","","","","","","","",""])

ifile = open('Output1.csv', "r")
reader = csv.reader(ifile, delimiter = ';')

rownum = 0

for row in reader:
    if rownum > 1:
        writer.writerow(row[2:16])
    rownum+=1

ifile.close()
ofile.close()

print ("Finished as Output2.csv")

#############################################################################
########################### Prepare Marker ##################################
#############################################################################
print ("Preparing marker file...")

ifile = open('Output1.csv', "r")
reader = csv.reader(ifile, delimiter = ';')
ofile = open('Output3.csv', 'w', newline = "")
writer = csv.writer(ofile, delimiter = '\t')

rownum = 0
holder = 0
newer = 0

writer.writerow(["TL02"])

for row in reader:
    if rownum < 2:
        pass
    else:
        if row[16] != "":
            newer = float(row[16].replace(',' , '.'))
            if newer-holder == 0:
                pass
            else:
                if newer == 1:
                    text = "neut"
                elif newer == 2:
                    text = "low"
                elif newer == 3:
                    text = "high"
                elif newer == 4:
                    text = "chill"
                elif newer == 0:
                    text = "noresp"
                writer.writerow([str(rownum-1),str(rownum-1),text])
                holder=newer

    rownum += 1

ifile.close()
ofile.close()

#############################################################################
################# Prepare Additional Marker without 0 #######################
#############################################################################


print ("Preparing additional marker file without no response...")

ifile = open('Output2.csv', "r")
reader = csv.reader(ifile, delimiter = '\t')
row_count = sum(1 for row in reader) - 1
ifile.close()

ifile = open('Output3.csv', "r")
reader = csv.reader(ifile, delimiter = '\t')
ofile = open('Output4.csv', 'w', newline = "")
writer = csv.writer(ofile, delimiter = '\t')

rownum = 0
scribble = [""]*3
tray = [""]*3

for row in reader:
    scribble = tray
    tray = row
    #Process the rest
    if rownum > 1:
        writer.writerow(scribble)
        gap = int(tray[0]) - int(scribble[0])
        mark = int(gap/(hz*epoch))
        for i in range (mark):
            nextp = int(scribble[0])+((hz*epoch)*(i+1))
            placeholder = scribble[2] + "M"
            if placeholder != "norespM":
                writer.writerow([str(nextp),str(nextp),placeholder])

        # if scribble[2][0] == 'f' or scribble[2][0] == 'i' or scribble[2][0] == 'n':
        #     gap = int(tray[0]) - int(scribble[0])
        #     mark = int(gap/hz)
        #     for i in range (mark):
        #         next = int(scribble[0])+(hz*(i+1))
        #         placeholder = scribble[2][0:4] + "M"
        #         writer.writerow([str(next),str(next+1),placeholder])


    #Skip header
    if rownum == 0:
        writer.writerow(row)

    rownum += 1

    
writer.writerow(tray)
gap = row_count - int(tray[0])
mark = int(gap/(hz*epoch))
for i in range (mark):
    nextp = int(tray[0])+((hz*epoch)*(i+1))
    placeholder = tray[2] + "M"
    writer.writerow([str(nextp),str(nextp),placeholder])


ifile.close()
ofile.close()
print ("Finished as Output4.csv")






#############################################################################
################### Prepare Additional Marker with 0 ########################
#############################################################################


print ("Preparing additional marker file with no response...")

ifile = open('Output2.csv', "r")
reader = csv.reader(ifile, delimiter = '\t')
row_count = sum(1 for row in reader) - 1
ifile.close()

ifile = open('Output3.csv', "r")
reader = csv.reader(ifile, delimiter = '\t')
ofile = open('Output5.csv', 'w', newline = "")
writer = csv.writer(ofile, delimiter = '\t')

rownum = 0
scribble = [""]*3
tray = [""]*3

for row in reader:
    scribble = tray
    tray = row
    #Process the rest
    if rownum > 1:
        writer.writerow(scribble)
        gap = int(tray[0]) - int(scribble[0])
        mark = int(gap/(hz*epoch))
        for i in range (mark):
            nextp = int(scribble[0])+((hz*epoch)*(i+1))
            placeholder = scribble[2] + "M"
            writer.writerow([str(nextp),str(nextp),placeholder])

        # if scribble[2][0] == 'f' or scribble[2][0] == 'i' or scribble[2][0] == 'n':
        #     gap = int(tray[0]) - int(scribble[0])
        #     mark = int(gap/hz)
        #     for i in range (mark):
        #         next = int(scribble[0])+(hz*(i+1))
        #         placeholder = scribble[2][0:4] + "M"
        #         writer.writerow([str(next),str(next+1),placeholder])


    #Skip header
    if rownum == 0:
        writer.writerow(row)

    rownum += 1

    
writer.writerow(tray)
gap = row_count - int(tray[0])
mark = int(gap/(hz*epoch))
for i in range (mark):
    nextp = int(tray[0])+((hz*epoch)*(i+1))
    placeholder = tray[2] + "M"
    writer.writerow([str(nextp),str(nextp),placeholder])


ifile.close()
ofile.close()
print ("Finished as Output5.csv")
print ("All operation finished.")
print ("Pipeline was closed.")